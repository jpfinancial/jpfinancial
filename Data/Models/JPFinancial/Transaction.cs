﻿using System;
using System.ComponentModel.DataAnnotations;
using JPFData.Enumerations;
using JPFData.Interfaces;

namespace JPFData.Models.JPFinancial
{
    public class Transaction : ITransaction
    {
        public Transaction()
        {
            UserId = Global.Instance.User == null ? string.Empty : Global.Instance.User.Id;
            Date = DateTime.Today;
            Payee = string.Empty;
            Memo = string.Empty;
            Amount = 0.0m; ;
        }

        [Required, Key]
        public int Id { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required, StringLength(255)]
        public string Payee { get; set; }

        [StringLength(255)]
        public string Memo { get; set; }

        [Required]
        public TransactionTypesEnum Type { get; set; }

        [Required]
        public CategoriesEnum Category { get; set; }

        public int? CreditAccountId { get; set; }
        public int? DebitAccountId { get; set; }
        public int? SelectedExpenseId { get; set; }

        [Required, DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [Display(Name = "From")]
        public Account CreditAccount { get; set; }

        [Display(Name = "To")]
        public Account DebitAccount { get; set; }

        [Required, DataType(DataType.Currency)]
        public decimal Amount { get; set; }

        [Required, Display(Name = "Charged to Credit Card?")]
        public bool UsedCreditCard { get; set; }
    }
}